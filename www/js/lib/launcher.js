/**
 * Launcher
 *
 * @Author Daniel A. Lepe Ayala
 * @Date 18/Oct/2016
 * @Version 1.0
 */

// FILE INJECTOR
var injectScript = function(file) {
        var s = document.createElement('script');
        s.setAttribute('src', '' + 'js/' + file + '.js');
        s.setAttribute('type', 'text/javascript');
        s.setAttribute('async', 'async');
        document.body.appendChild(s);
    },

    // lOAD GLOBALS
    loadGlobals = function() {
        var position = 'globals/';
        // LAUNCH LOOP
        for (i in Tree.Globals)
            injectScript(position + Tree.Globals[i]);
        // RETURN SOMETHING!
        return true;
    },

    // lOAD COMMONS
    loadCommons = function() {
        var position = 'commons/';
        // LAUNCH LOOP
        for (i in Tree.Commons)
            injectScript(position + Tree.Commons[i] + ".service");
        // RETURN SOMETHING!
        return true;
    },

    // lOAD CONTROLLERS
    loadControllers = function() {
        var position = 'controllers/';
		// LOADS MAIN CONTROLLER
		injectScript(position + 'Main' + 'Controller');
		injectScript(position + 'Menu' + 'Controller');
        // LAUNCH LOOP
        for (i in Tree.Controllers)
            injectScript(position + ((Tree.Controllers[i]).controller) + 'Controller');
        // RETURN SOMETHING!
        return true;
    },

    // lOAD DIRECTIVES
    loadDirectives = function() {
        var position = 'directives/';
        // LAUNCH LOOP
        for (i in Tree.Directives)
            injectScript(position + Tree.Directives[i] + ".directive");
        // RETURN SOMETHING!
        return true;
    };

// FIRE THEM ALL!
(function() {
    // injectScript('app.bootstrap');
    loadGlobals();
    loadCommons();
    loadControllers();
    loadDirectives();
}());
