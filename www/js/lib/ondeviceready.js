// ON DEVICE READY.
onDeviceReady = function() {
    console.log('Device Ready!');
    // REQUIRED TO ADD SPACE UNDER ICON AND NETWORK INTERFACES
    if (device.platform == "iOS") $('html').addClass('padding-ios');
    // ANGULAR BOOTSRAP
    $(function() {
        angular.bootstrap(document, ['Core']);
    });
    // HIDES SPLASHSCREEN
    navigator.splashscreen.hide();
};

// STARTUP ATTACHMENT!
document.addEventListener("deviceready", onDeviceReady, false);
