(function() {
    /**
     *
     * Modulo PUSH para implementación con Angular.
     *
     * @ Daniel Lepe
     * @ Version 1.0
     * @ Date 03/10/2016
     */
    var PushService = angular.module('PushService', ['DbService']);

    PushService.service("hearPush", function($location, $rootScope, db) {

        this.init = function() {
            //FCMPlugin.getToken( successCallback(token), errorCallback(err) ); 
            //Keep in mind the function will return null if the token has not been established yet. 
            FCMPlugin.getToken(  function(token) {    
                console.log(token);  
                db.setItem('device_token', token);
            },   function(err) {    
                alert('error retrieving token: '  +  err);  
            });
        };

        this.hear = function() {
            //Here you define your application behaviour based on the notification data. 
            FCMPlugin.onNotification(  function(data) {
                $location.path('/toHome');
                $rootScope.$apply();
                navigator.notification.alert(data.body, function() {}, data.title, 'Ok');
            },   function(msg) {    
                console.log('onNotification callback successfully registered: '  +  msg);  
            },   function(err) {    
                alert('Error registering onNotification callback: '  +  err);  
            });
        };

    });

    PushService.service('singIn', function($http, db) {
        // UBICACIÓN FÍSICA DE LA API
        this.apiURI = Globals.api.server;
        this.apiContext = Globals.api.context;
        this.header = {
            method: 'POST',
            headers: {
                'Content-Type': 'text/json'
            },
            timeout: 50000
        };
        // SETS ACTION FOR SIGN IN DEVICE
        this.signInDeviceAction = "push/devicessignin";
        // REGISTRO DEL DISPOSITIVO EN LA API
        this.exec = function() {
            // DEVICE TOKEN
            var device_token = db.getItem('device_token'),
                current_config = this.header;
            //Build current config
            current_config.url = this.apiURI + "/"
            this.apiContext + "/" + this.signInDeviceAction;
            // VALIDATE IS STRING
            if (device_token == undefined) {
                console.log("No se puede registrar el dispositivo en la API, debido a que el token es nulo.");
                console.log("this.device_token");
                console.log(device_token);
                return false;
            }
            // CASOS DE USO
            function onSuccess(data) {
                // console.log(data.data);
                // alert(data.data.msg);
            }
            // PROCESS DATA LOADED FROM db
            function processDta(userData) {
                // INIT
                var signindeviceObject = {
                    "type": "usuarios",
                    "email": userData.eMail,
                    "nombres": userData.NOMBRE,
                    "device_token": device_token,
                    "appIdentifier": db.getItem('packageName'),
                    "platform": db.getItem('platform'),
                    "app_version": db.getItem('versionNumber')
                };
                //Set Data
                current_config.data = JSON.stringify(signindeviceObject);
                //Perform $http
                return $http(current_config).then(onSuccess, onSuccess);
            }

            db.readLogin().then(processDta);
        };
    });

    PushService.run(function($http, hearPush, db) {
        cordova.getAppVersion.getPackageName().then(function(packageName) {
            db.setItem('packageName', packageName);
        });
        hearPush.init();
        hearPush.hear();
    });

})();
