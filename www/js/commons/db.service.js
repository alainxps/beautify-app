(function() {
    /**
     *
     * Modulo LocalStorage DB para implementación con Angular.
     *
     * @ Daniel Lepe
     * @ Version 1.0
     * @ Date 19/10/2016
     */
    var DbService = angular.module('DbService', []);

    DbService.service('db', function() {

        this.setItem = function(itemLocalStorage, content) {
            localStorage.setItem(itemLocalStorage, JSON.stringify(content));
            return content;
        };

        this.getItem = function(itemLocalStorage) {
            return (!!localStorage.getItem(itemLocalStorage)) ? JSON.parse(localStorage.getItem(itemLocalStorage)) : {};
        };

        this.removeItem = function(itemLocalStorage) {
            localStorage.removeItem(itemLocalStorage);
        };

    });
})();
