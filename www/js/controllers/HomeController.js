/**
 * Home Controler
 *
 * @Author [names; emails]
 * @Date [date]
 */
(function() {
    'use strict';

    // Registro de controlador en módulo.
    angular
        .module('Core')
        .controller('HomeCtrl', HomeCtrl);

    // Inyección al controlador.
    HomeCtrl.$inject = ['$state'];

    function HomeCtrl($state) {
		var $view = this;
        $view.next=function () {
            console.log("Ir a => Registro");
            $state.go("Registro");
        };
        $view.categorias=function () {
            console.log("Ir a => Categorias");
            $state.go("Categorias");
        };
    };

}());
