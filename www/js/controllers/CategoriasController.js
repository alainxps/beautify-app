/**
 * Categorias Controler
 *
 * @Author [names; emails]
 * @Date [date]
 */
(function() {
    'use strict';

    // Registro de controlador en módulo.
    angular
        .module('Core')
        .controller('CategoriasCtrl', CategoriasCtrl);

    // Inyección al controlador.
    CategoriasCtrl.$inject = ['$state'];

    function CategoriasCtrl($state) {
		var $view = this;
		$view.next=function () {
            console.log("entra => Registro");
            $state.go("Registro");
        };
    };

}());