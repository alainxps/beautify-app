/**
 * Main Controler
 *
 * @Author Daniel Albero Lepe
 * @Date [date]
 */
(function() {
    'use strict';

    // Registro de controlador en módulo.
    angular
        .module('Core')
        .controller('MainController', MainController);

    // Inyección al controlador.
    MainController.$inject = ['$state', '$scope'];

    function MainController($state, $scope) {
		var $view = this;
		$scope.$on('UIRollUP', function(event, data){
			console.log('MainController UIRollUP!');
			$scope.$broadcast('UIShowMenu', "algo");
		});
    };

}());
