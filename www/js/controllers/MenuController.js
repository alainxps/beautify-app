/**
 * Main Controler
 *
 * @Author Daniel Albero Lepe
 * @Date [date]
 */
(function() {
    'use strict';

    // Registro de controlador en módulo.
    angular
        .module('Core')
        .controller('MenuController', MenuController);

    // Inyección al controlador.
    MenuController.$inject = ['$state', '$scope'];

    function MenuController($state, $scope) {
		$scope.showing = 0;
		$scope.goto = function(location){
			console.log("Going To:");
			console.log(location);
			$state.go(location);
			$scope.showing = 0;
		};
		$scope.$on('UIShowMenu', function(event, data){
			$scope.showing = (!$scope.showing);
		});
		$scope.stopShowing = function(){
			$scope.showing = 0;
		};
    }

}());