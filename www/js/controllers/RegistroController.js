/**
 * Registro Controller
 *
 * @Author [names; emails]
 * @Date [date]
 */
(function() {
    'use strict';

    // Registro de controlador en módulo.
    angular
        .module('Core')
        .controller('RegistroCtrl', RegistroCtrl);

    // Inyección al controlador.
    RegistroCtrl.$inject = ['$state'];

    function RegistroCtrl($state) {
		var $view = this;
		$view.next=function () {
            console.log("entra => ");
            $state.go("/Registro");
        };
    };

}());