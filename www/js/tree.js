// INIT
var Tree = {
    // Controllers
    Controllers: [
		// HOME CONTROLLER
		{
	        controller			: "Home",
	        view				: "home.html"
	    },
		// Registro CONTROLLER
		{
	        controller			: "Registro",
	        view				: "registro.html"
	    },
		// Categorias CONTROLLER
		{
	        controller			: "Categorias",
	        view				: "categorias.html"
	    },
		// NEXT ONES
	],
	Directives: [
		'top-header',
		'sidebar-menu'
	],
	Commons: [
		'db',
		'push',
    ],
	Globals: [
		'app.globals'
    ]
};
