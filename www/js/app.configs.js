/**
 * App Principal
 *
 * Debe contener los datos Angular.bootsrap para la aplicación.
 *
 * @Author Daniel Alberto Lepe Ayala
 * @Date 19/10/2016
 */
(function() {
    // GETS MODULE
    angular.module('Core')

    // CONFIGURACIONES
    .config(function($stateProvider, $urlRouterProvider) {
        // ROUTE LOOP
        console.log(Tree.Controllers);
        for (t in Tree.Controllers) {
            var Location = Tree.Controllers[t],
                config = {
                    url: '/' + Location.controller,
                    controller: (Location.controller + 'Ctrl'),
                    controllerAs: Location.controller,
                    templateUrl: 'views/' + Location.view,
                };
            console.log("config");
            console.log(config);
            $stateProvider.state(Location.controller, config);
        }
        // HOME
        $urlRouterProvider.otherwise('/Home');
    })

    // RUN
    .run(function($rootScope, $location) {
        console.log("Main App Loaded!");
    });
}());