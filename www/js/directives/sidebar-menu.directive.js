/**
 *
 * Directiva de barra superior.
 *
 * @ Daniel Lepe
 * @ Version 1.0
 * @ Date 20/10/2016
 */
(function(){
	angular.module('Core').directive('sidebarMenu', SidebarMenu);
	 function SidebarMenu () {
		return {
			restrict: "E",
			replace: true,
			scope: {
				showing: "="
			},
			templateUrl: 'views/elements/sidebar-menu.html',
			controller: ['$scope', function($scope){

				
			}]
		};
	}
}());
