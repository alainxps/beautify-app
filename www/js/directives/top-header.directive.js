/**
 *
 * Directiva de barra superior.
 *
 * @ Daniel Lepe
 * @ Version 1.0
 * @ Date 20/10/2016
 */
(function(){
	angular.module('Core').directive('topHeader', topHeader);
	// angular.$inject()
	 function topHeader () {
		return {
			restrict: "E",
			replace: true,
			scope: {
				searchPlaceholder: "@",
				searchTitle: "@"
				// showMenu: "@"
			},
			templateUrl: 'views/elements/top-header.html',
			controller: ['$scope', function($scope){
				$scope.showMenu =  function (){
					$scope.$emit('UIRollUP', 'toggle');
				};
				// console.log($scope.searchTitle);
			}]
		};
	}
}());
