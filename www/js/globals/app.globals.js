/**
 * Lista de modelos internos de la Core.
 *
 * @Authors [names; emails]
 * @Date [date]
 */
var Globals = {
        token: null,
        debug: true,
        user: {},
        api: {}
    },
	UI		= {
		layout: {
			menu: false
		}
	},
    device = device || {
        platform: "browser"
    };

;
(function() {
    // GLOBALS
    Globals.api.server = "com.server.api";
    Globals.api.context = "api";
}());
